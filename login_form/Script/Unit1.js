﻿function create_new_user_A()
{
  //Clicks the 'buttonCreateNew' control.
  Aliases.browser.pageUitoolkit2.buttonCreateNew.ClickButton();
  //Clicks at point (107, 10) of the 'textboxReferenceListName' object.
  Aliases.browser.pageUitoolkit3.form.textboxReferenceListName.Click(107, 10);
  //Sets the text 'vuho1' in the 'textboxReferenceListName' text editor.
  Aliases.browser.pageUitoolkit3.form.textboxReferenceListName.SetText("vuho1");
  //Clicks at point (171, 17) of the 'textboxDescription' object.
  Aliases.browser.pageUitoolkit3.form.textboxDescription.Click(171, 17);
  //Sets the text 'vuho demo testing' in the 'textboxDescription' text editor.
  Aliases.browser.pageUitoolkit3.form.textboxDescription.SetText("vuho demo testing");
  //Clicks at point (344, 10) of the 'selectProject' object.
  Aliases.browser.pageUitoolkit3.form.panelFormGroup.selectProject.Click(344, 10);
  //Clicks at point (65, 207) of the 'form' object.
  Aliases.browser.pageUitoolkit3.form.Click(65, 207);
  //Selects the 'INT (0-99)' item of the 'selectProject' select control.
  Aliases.browser.pageUitoolkit3.form.panelFormGroup2.selectProject.ClickItem("INT (0-99)");
  //Selects the item, specified as an index, from 0, or a name.
  Aliases.browser.pageUitoolkit3.form.panelFormGroup2.selectProject.ClickItem("INT (100-256=0)");
  //Clicks the 'buttonSave' control.
  //Aliases.browser.pageUitoolkit3.form.buttonSave.ClickButton();
  //Checks whether the 'wText' property of the Aliases.browser.pageUitoolkit3.form.panelFormGroup2.selectProject object equals 'INT (0-99)'.
  aqObject.CheckProperty(Aliases.browser.pageUitoolkit3.form.panelFormGroup2.selectProject, "wText", cmpEqual, "INT (0-99)");
}

function create_new_user(Param1, Param2)
{
  var testRefer, testDescription, selectProp;
  testRefer = "vuho";
  testDescription = "vuho descript";
  selectProp = "INT (0-99)";
  //Clicks the 'buttonCreateNew' control.
  Aliases.browser.pageUitoolkit2.buttonCreateNew.ClickButton();
  //Clicks at point (107, 10) of the 'textboxReferenceListName' object.
  Aliases.browser.pageUitoolkit3.form.textboxReferenceListName.Click(107, 10);
  //Sets the text KeywordTests.create_new_user.Variables.testRefer in the 'textboxReferenceListName' text editor.
  Aliases.browser.pageUitoolkit3.form.textboxReferenceListName.SetText(testRefer);
  //Clicks at point (171, 17) of the 'textboxDescription' object.
  Aliases.browser.pageUitoolkit3.form.textboxDescription.Click(171, 17);
  //Sets the text in the 'textboxDescription' text editor.
  Aliases.browser.pageUitoolkit3.form.textboxDescription.SetText(Param2);
  //Clicks at point (344, 10) of the 'selectProject' object.
  Aliases.browser.pageUitoolkit3.form.panelFormGroup.selectProject.Click(344, 10);
  //Clicks at point (65, 207) of the 'form' object.
  Aliases.browser.pageUitoolkit3.form.Click(65, 207);
  //Selects the KeywordTests.create_new_user.Variables.selectProp item of the 'selectProject' select control.
  Aliases.browser.pageUitoolkit3.form.panelFormGroup2.selectProject.ClickItem(selectProp);
  //Selects the item, specified as an index, from 0, or a name.
  Aliases.browser.pageUitoolkit3.form.panelFormGroup2.selectProject.ClickItem("INT (100-256=0)");
  //Clicks the 'buttonSave' control.
  //Aliases.browser.pageUitoolkit3.form.buttonSave.ClickButton();
  //Checks whether the 'wText' property of the Aliases.browser.pageUitoolkit3.form.panelFormGroup2.selectProject object equals 'INT (0-99)'.
  aqObject.CheckProperty(Aliases.browser.pageUitoolkit3.form.panelFormGroup2.selectProject, "wText", cmpEqual, "INT (0-99)");
}